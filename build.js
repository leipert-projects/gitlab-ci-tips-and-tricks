const fs = require('fs');
const path = require('path');
const MarkdownIt = require('markdown-it');

const md = MarkdownIt({
  html: false,
  linkify: true,
  typographer: true,
  breaks: true,
  xhtmlOut: false,
});

const MermaidPlugIn = (md, opts)=> {
  const defaultRenderer = md.renderer.rules.fence.bind(md.renderer.rules);

  md.renderer.rules.fence=(tokens, idx, opts, env, self)=>{
    const token = tokens[idx]
    if (token.info === 'mermaid') {
      const code = token.content.trim()
      return `<div class="mermaid">${code}</div>`
    }
    return defaultRenderer(tokens, idx, opts, env, self)
  }
};

md.use(MermaidPlugIn)

const src = fs.readFileSync(path.join(__dirname, 'README.md'), 'utf8');
const body = md.render(src);

const html = `
<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <title>GitLab CI – Tips and Trocks</title>
  <meta name="author" content="Lukas Eipert">

  <link rel="stylesheet" href="css/styles.css?v=1.0">

</head>

<body>
  ${body}
  <script src="https://unpkg.com/mermaid@8.4.8/dist/mermaid.min.js"></script>
  <script>mermaid.initialize({startOnLoad:true});</script>
</body>
</html>
`;

fs.writeFileSync(path.join(__dirname, 'dist', 'index.html'), html);

console.log('Successfully converted README')
