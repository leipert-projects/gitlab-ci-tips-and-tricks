# gitlab-ci-tips-and-tricks

## Introduction

GitLab CI has a pretty simple base structure:

```mermaid
graph RL
  subgraph test
    TEST:::stage
    lint-md:::immediate
  end
  subgraph build
    BUILD:::stage
    build_page
  end
  subgraph deploy
    DEPLOY:::stage
    pages
  end
  DEPLOY --> BUILD --> TEST
  classDef stage fill:#ecb613
  classDef immediate fill:#80ff00
```

```yaml
stages:
  - test
  - build
  - deploy

image: "node:12"

lint-md:
  stage: test
  script:
    - yarn install
    - yarn lint-md

build_page:
  stage: build
  script:
    - yarn install
    - yarn build
  artifacts:
    paths:
      - dist

pages:
  stage: deploy
  script:
    - mv dist public
    - ls -a public
  artifacts:
    paths:
      - public
```

## Hidden jobs && extending jobs

Job definitions starting with a dot won't be executed.
There exists an `extends:` key which can be used to extend job definitions. 

```yaml
stages:
  - test
  - build
  - deploy

.node:
  image: "node:12"

.yarn_install:
  before_script:
    - yarn install

lint-md:
  extends:
    - .node
    - .yarn_install
  stage: test
  script:
    - yarn lint-md

build_page:
  extends:
    - .node
    - .yarn_install
  stage: build
  script:
    - yarn build
  artifacts:
    paths:
      - dist

pages:
  extends: .node
  stage: deploy
  script:
    - mv dist public
    - ls -a public
  artifacts:
    paths:
      - public
```

# Caching and yaml anchors

GitLab supports caching and yaml anchors. This is a great way to reduce build time and reduce network load.

```yaml
stages:
  - test
  - build
  - deploy

.node:
  image: "node:12"

.cache: &global_cache
  key: node_modules
  paths:
    - node_modules/
  policy: pull

.yarn_install:
  before_script:
    - yarn install
  cache:
    <<: *global_cache

lint-md:
  stage: test
  extends:
    - .node
    - .yarn_install
  script:
    - yarn lint-md

build_page:
  extends:
    - .node
    - .yarn_install
  stage: build
  cache:
    <<: *global_cache
    policy: pull-push
  script:
    - yarn build
  artifacts:
    paths:
      - dist

pages:
  extends: .node
  stage: deploy
  script:
    - mv dist public
    - ls -a public
  artifacts:
    paths:
      - public
```

## Use of CI variables

CI Variables can be used to control the flow. They can even be used creatively for selecting images or building them

```mermaid
graph RL
  subgraph test
    TEST:::stage
    node:8:::immediate
    node:10:::immediate
    node:12:::immediate
  end
  subgraph build
    BUILD:::stage
    build_page
  end
  subgraph deploy
    DEPLOY:::stage
    pages
  end
  DEPLOY --> BUILD --> TEST
  classDef stage fill:#ecb613
  classDef immediate fill:#80ff00
```

```yaml
stages:
  - test
  - build
  - deploy

image: "alpine:latest"

.yarn_install:
  image: "node:12"
  before_script:
    - yarn install

.test:
  extends: .yarn_install
  image: "${CI_JOB_NAME}"
  stage: test
  script:
    - yarn test

node:10:
  extends: .test

node:12:
  extends: .test

node:latest:
  extends: .test

build_page:
  extends: .yarn_install
  stage: build
  script:
    - yarn build
  artifacts:
    paths:
      - dist

pages:
  stage: deploy
  script:
    - mv dist public
    - ls -a public
  artifacts:
    paths:
      - public
```

[Another example](https://gitlab.com/gitlab-org/gitlab-build-images/-/blob/master/.gitlab-ci.yml):

```yaml
.build_and_deploy_custom: &build_and_deploy_custom
  stage: build
  script:
    - ./scripts/custom-docker-build $CI_JOB_NAME -t "$CI_REGISTRY_IMAGE:$CI_JOB_NAME"
    - docker push "$CI_REGISTRY_IMAGE:$CI_JOB_NAME"
  only:
    - master

ruby-2.6-golang-1.13-git-2.21: *build_and_deploy_custom
ruby-2.6-golang-1.12-git-2.22: *build_and_deploy_custom
ruby-2.6-golang-1.13-git-2.22: *build_and_deploy_custom
ruby-2.6-golang-1.12-git-2.24: *build_and_deploy_custom
ruby-2.6-golang-1.13-git-2.24: *build_and_deploy_custom
```

## Quiz time:

```mermaid
graph RL
  subgraph test
    TEST:::stage
    A30:::immediate
    B10:::immediate
    C10:::immediate
  end
  subgraph build
    BUILD:::stage
    A10
    B30
    C20
  end
  subgraph deploy
    DEPLOY:::stage
    A20
    B20
    C30
  end
  A20 --> A10 --> A30
  B20 --> B30 --> B10
  C30 --> C20 --> C10
  DEPLOY --> BUILD --> TEST
  classDef stage fill:#ecb613
  classDef immediate fill:#80ff00
```
